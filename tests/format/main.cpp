#include <iostream>
#include <format>
#include <string>
#include <chrono>
#include <cmath>


int main(int argc, char **argv) {
  std::cout << std::format("Hello, {:s}!", "std::format") << std::endl;
  std::cout << std::format("Container: {}\n", std::vector<int>({3, 2, 1, 8, 5}));
  std::cout << std::format("Chrono: {}\n", std::chrono::system_clock::now());
  std::cout << std::format("Floats: {:.3}\n", M_PI);
}