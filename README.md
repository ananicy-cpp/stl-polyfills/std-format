# STL format polyfill

A simple polyfill allowing you to use `std::format` even if your compiler doesn't support it yet (as of writing this, NONE do).

## Usage

```cmake
include(FetchContent)

FetchContent_Declare(fmt_polyfill
        GIT_REPOSITORY https://gitlab.com/ananicy-cpp/stl-format-polyfill.git
        GIT_TAG 1aae1250948fb9df5f1e2b9b068fb725f37962e0)

FetchContent_MakeAvailable(fmt_polyfill)
target_link_libraries(<TARGET_NAME> PRIVATE stl_polyfill::format)
```